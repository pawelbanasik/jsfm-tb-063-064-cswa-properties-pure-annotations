package com.pawelbanasik.springdemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.pawelbanasik.springdemo.domain.Organization;

public class PropertiesWithAnnotationApp {
	public static void main(String[] args) {

		ApplicationContext context = new AnnotationConfigApplicationContext(PropertyConfig.class);
		Organization organization = (Organization) context.getBean("myorg");
		System.out.println(organization);
		System.out.println("Slogan: " + organization.corporateSlogan());
		((AbstractApplicationContext) context).close();

	}
}
