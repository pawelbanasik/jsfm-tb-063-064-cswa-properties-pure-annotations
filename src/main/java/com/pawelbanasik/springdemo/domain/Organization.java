
package com.pawelbanasik.springdemo.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.pawelbanasik.springdemo.service.BusinessService;

@Component("myorg")
public class Organization {

	@Value("${nameOfCompany}")
	private String companyName;

	@Value("${startUpYear}")
	private int yearOfIncorporation;

	@Value("${postalCode}")
	private String postalCode;

	// nadaje defaultowa wartosc
	@Value("${empCount:22222}")
	private String employeeCount;

	// nadaje defaultowa ale z pliku jest wazniejsza
	@Value("${corporateSlogan:We build world class software!}")
	private String slogan;

	private BusinessService businessService;

	private String missionStatement;
	
	// provides getter methods to read the indivudual properties into an application
	@Autowired
	private Environment env;
	
	
	public Organization() {
		System.out.println("Default contstructor is called. ");
	}

	public Organization(String companyName, int yearOfIncorporation) {
		this.companyName = companyName;
		this.yearOfIncorporation = yearOfIncorporation;
		System.out.println("Constructor called.");
	}

	// alternative way of retrieving propery without @Value annotation
	public String corporateSlogan() {
		missionStatement = env.getProperty("statement");
		return missionStatement + " and also " + slogan;
	}
	//
	// public void initialize() {
	// System.out.println("Organization: initialize method called.");
	// }
	//
	// public void destroy() {
	// System.out.println("Organization: destroy method called.");
	// }

	// public void corporateSlogan() {
	// String slogan = "We build the ultimate driving machines";
	// System.out.println(slogan);
	// }

	public void postConstruct() {
		System.out.println("organization: postConstruct called.");
	}

	public void preDestroy() {
		System.out.println("organization: preDestroy called.");
	}

	// accesses the buisness service and executes the office service methods
	// after the injection of the desired service implementation
	public String corporateService() {
		return businessService.offeringService(companyName);
	}

	@Override
	public String toString() {
		return "Organization [companyName=" + companyName + ", yearOfIncorporation=" + yearOfIncorporation
				+ ", postalCode=" + postalCode + ", employeeCount=" + employeeCount + "]";
	}

	public void setEmployeeCount(String employeeCount) {
		this.employeeCount = employeeCount;
		System.out.println("setEmployeeCount called.");
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
		System.out.println("setPostacCode called.");
	}

	// removes the hard coding (has nothing to do with DI)
	public void setSlogan(String slogan) {
		this.slogan = slogan;
		System.out.println("setSlogan called.");
	}

	// this will help us do the dependency injection along with some configuration
	// spring config file
	// you need this setter to create property in the xml file for beans
	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
		System.out.println("setBusinessService called.");
	}

}
