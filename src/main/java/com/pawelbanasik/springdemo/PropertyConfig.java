package com.pawelbanasik.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

// klasa zamiast configa w xml'u

@Configuration
@ComponentScan(basePackages = "com.pawelbanasik.springdemo")
@PropertySource(value = { "classpath:properties/organization.properties" })
public class PropertyConfig {

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();

	}

}
